var gulp = require('gulp'),
    sass = require('gulp-sass'),
    pug = require('gulp-pug'),
    autoprefixer = require('gulp-autoprefixer'),
    cssnano = require('gulp-cssnano'),
    jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify'),
    imagemin = require('gulp-imagemin'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    notify = require('gulp-notify'),
    cache = require('gulp-cache'),
    browserSync = require('browser-sync').create(),
    del = require('del');



/*Compile SCSS, run through Autoprefixer, rename .min, minify with cssnano, notify*/

gulp.task('sass', function() {
  return gulp.src('./src/css/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer('last 2 version'))
    .pipe(gulp.dest('./dist/css'))
    .pipe(rename({suffix: '.min'}))
    .pipe(cssnano())
    .pipe(gulp.dest('dist/css'))
    .pipe(notify({ message: 'Styles task complete' }))
    .pipe(browserSync.stream());
});


gulp.task('scripts', function() {
  return gulp.src('src/js/**/**/*.js')
    .pipe(jshint('.jshintrc'))
    .pipe(jshint.reporter('default'))
    .pipe(concat('main.js'))
    .pipe(gulp.dest('dist/js'))
    .pipe(rename({suffix: '.min'}))
    .pipe(uglify())
    .pipe(gulp.dest('dist/js'))
    .pipe(notify({ message: 'Scripts task complete' }))
    .pipe(browserSync.stream());
});


gulp.task('images', function() {
  return gulp.src('src/images/**/*')
    .pipe(cache(imagemin({ optimizationLevel: 5, progressive: true, interlaced: true })))
    .pipe(gulp.dest('dist/img'))
    .pipe(notify({ message: 'Images task complete' }));
});


gulp.task('html', function buildHTML() {
  return gulp.src('src/*.pug')
      .pipe(pug({
        pretty:true
      }))
      .pipe(gulp.dest('.'))
      .pipe(browserSync.stream());
});

gulp.task('clean', function() {
    return del(['dist/css', 'dist/js', 'dist/img', './index.html']);
});


/*Do all the things*/

gulp.task('default', ['clean'], function() {
    gulp.start('sass', 'scripts', 'images', 'html');
});


/*Watch all the things*/

gulp.task('watch', function() {

  // Watch .scss files
  gulp.watch('src/css/**/*.scss', ['sass']);

  // Watch .js files
  gulp.watch('src/js/**/*.js', ['scripts']);

  // Watch image files
  gulp.watch('src/images/**/*', ['images']);

});


// Static Server + watching scss/html files
gulp.task('serve', ['default'], function() {

    browserSync.init({
        server: "./"
    });

    gulp.watch("src/css/*.scss", ['sass']);
    gulp.watch("src/js/*.js", ['scripts']);
    gulp.watch("src/images/**/*", ['images']);
    gulp.watch("src/*.pug", ['html']);
    gulp.watch("index.html").on('change', browserSync.reload);
});

