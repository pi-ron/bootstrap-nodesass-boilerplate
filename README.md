# README #

Nodesass compatible bootstrap skeleton project with Pug (Jade) support, browsersync for dev server and gulp tasks for compiling all the things.

### What is this repository for? ###

* Quick starting bootstrap projects with nodesass

### How do I get set up? ###

`npm install`

`gulp serve`
